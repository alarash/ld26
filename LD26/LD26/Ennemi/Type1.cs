﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26.Ennemi
{
    class Type1 : EnnemiBase
    {
        public Vector2 direction;
        public int vitesse;
        public int vitesseMoy = 100; // px/s

        private static Random rnd = new Random();

        public Type1() { }
        public Type1(Type1 src):base(src)
        {
            //direction = src.direction;
            changeDirection();
            vitesse = vitesseMoy * rnd.Next(80, 120) / 100;
        }

        public override EnnemiBase Clone()
        { return new Type1(this); }

        private void changeDirection()
        {
            do
            {
                direction = new Vector2(rnd.Next(-100, 100) / 100f, rnd.Next(-100, 100) / 100f);
            } 
            while (direction.LengthSquared() == 0);
            direction.Normalize();
        }
        public override void Update(GameTime gameTime, Vector2 distance, Map map)
        {
            if (etat == Etat.normal)
            {
                Vector2 nextPos = location + direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * vitesse / 1000;

                bool canMove = true;
                foreach (int tileIndex in map.getHitsIndex(spriteEnnemie.hit, nextPos, spriteEnnemie.centre))
                    if (canMove)
                        canMove = map.tmap.getTileAt(tileIndex) == 0;

                if (canMove)
                    location = nextPos;
                else
                    changeDirection();
            }

            base.Update(gameTime, distance, map);
        }
    }
}
