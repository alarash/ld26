﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26.Ennemi
{
    class Type2 : EnnemiBase
    {
        public Vector2 direction;
        public int vitesse;
        public int vitesseMoy = 150; // px/s

        private static Random rnd = new Random();

        public Type2() { }
        public Type2(Type2 src):base(src)
        {
            //direction = src.direction;
            direction = new Vector2();
            vitesse = vitesseMoy * rnd.Next(80, 120) / 100;
        }

        public override EnnemiBase Clone()
        { return new Type2(this); }

        public override void Update(GameTime gameTime, Vector2 distance, Map map)
        {
            if (etat == Etat.normal)
            {
                if (direction.LengthSquared() == 0)
                {
                    direction = distance;
                    direction.Normalize();
                }
                Vector2 nextPos = location + direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * vitesse / 1000;

                bool canMove = true;
                foreach (int tileIndex in map.getHitsIndex(spriteEnnemie.hit, nextPos, spriteEnnemie.centre))
                    if (canMove)
                        canMove = map.tmap.getTileAt(tileIndex) == 0;

                if (canMove)
                    location = nextPos;
                else
                {
                    direction = -distance;
                    direction.Normalize();
                }
            }
            base.Update(gameTime, distance, map);
        }

    }
}
