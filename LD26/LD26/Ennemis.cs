﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26
{
    class Ennemis
    {
        private Dictionary<String, EnnemiBase> types = new Dictionary<String, EnnemiBase>();
        private List<EnnemiBase> _items = new List<EnnemiBase>();
        public EnnemiBase[] items
        { get { return _items.ToArray(); } } 

        private static Random rnd = new Random();

        public SoundEffect sndExplosion;

        public void Initialize()
        {
        }

        public void LoadContent(ContentManager Content)
        {
            AnimedSprite explosion = AnimedSprite.Load(Content, "Objects/explosion01");

            types["type1"] = new Ennemi.Type1() 
            { 
                spriteEnnemie = AnimedSprite.Load(Content, "Objects/ENEMY01"),
                spriteExplosion = explosion
            };
            types["type2"] = new Ennemi.Type2() 
            { 
                spriteEnnemie = AnimedSprite.Load(Content, "Objects/ENEMY02"),
                spriteExplosion = explosion
            };
            types["type3"] = new Ennemi.Type3() 
            {
                spriteEnnemie = AnimedSprite.Load(Content, "Objects/ENEMY03"),
                spriteExplosion = explosion
            };

            sndExplosion = Content.Load<SoundEffect>("sounds/snd_explosion01");
        }

        public void UnloadContent()
        {

            sndExplosion.Dispose();
        }

        public void Update(GameTime gameTime, Map map)
        {
            float diag = map.windowCenter.LengthSquared();
            for (int i = 0; i < _items.Count; i++)
            {
                Vector2 dis = _items[i].location - map.location ;
                if (dis.LengthSquared() > diag)
                    _items.RemoveAt(i--);
                else
                    _items[i].Update(gameTime, dis, map);
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 offset)
        {
            foreach (EnnemiBase item in _items)
                item.Draw(gameTime, spriteBatch, -offset); // item.location - offset);
        }

        public void Add(String formes, Vector2 location)
        {
            String[] forme = formes.Split(new char[] { ',', ';' });
            EnnemiBase ennemi = types[forme[rnd.Next(forme.Length)]].Clone();
            ennemi.location = location;

            _items.Add(ennemi);
        }

        public void Remove(EnnemiBase item)
        { _items.Remove(item); }
        public void Remove(System.Collections.Generic.ICollection<EnnemiBase> toRemove)
        { foreach (EnnemiBase item in toRemove) _items.Remove(item); }
        public void Clear()
        { _items.Clear(); }

        /*
        public List<PowerUp> hit(Rectangle hitBox)
        {
            List<PowerUp> ret = new List<PowerUp>();
            foreach (PowerUp item in items)
            {
                Rectangle spHit = item.sprite.hit;
                spHit.X += (int)(item.location.X - item.sprite.centre.X);
                spHit.Y += (int)(item.location.Y - item.sprite.centre.Y);

                if (hitBox.Intersects(spHit))
                    ret.Add(item);
            }
            return ret;
        }
        */

    }
}
