using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class GameLD26 : Microsoft.Xna.Framework.Game
    {
        protected GraphicsDeviceManager graphics;
        protected SpriteBatch spriteBatch;

        private KeyboardState oldKeyboardState;
        private KeyboardState curKeyboardState;
        private MouseState oldMouseState;
        private MouseState curMouseState;
        private GamePadState oldGamePadState;
        private GamePadState curGamePadState;

        private Map ZeMap;
        private Menu menu;

        protected Vector2 windowSize;
        protected Vector2 windowCenter;

        private Texture2D background;

        public SoundEffect musIntro;
        public SoundEffect musPlay;
        public SoundEffect musGameOver;


        private SoundEffectInstance currentMusicInst;

        public enum Ecran
        { Jeu, Menu }
        public Ecran ecran;
        public GameLD26()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            Content.RootDirectory = "Content";

            //MediaPlayer.IsRepeating = true;
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            oldKeyboardState = Keyboard.GetState();
            oldMouseState = Mouse.GetState();
            oldGamePadState = GamePad.GetState(PlayerIndex.One);

            windowSize = new Vector2(
                this.GraphicsDevice.Viewport.Width, 
                this.GraphicsDevice.Viewport.Height);
            windowCenter = windowSize / 2;

            ZeMap = new Map(this);
            ZeMap.Initialize();

            menu = new Menu(this);
            menu.Initialize();

            ZeMap.windowCenter = windowCenter;
            ZeMap.windowSize = windowSize;
            menu.windowCenter = windowCenter;
            menu.windowSize = windowSize;

            ecran = Ecran.Menu;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            background = Content.Load<Texture2D>("background01");

            musIntro = Content.Load<SoundEffect>("sounds/LDIntro");
            musPlay = Content.Load<SoundEffect>("sounds/LDMetalTest02");
            musGameOver = Content.Load<SoundEffect>("sounds/LDGameOver");


            ZeMap.LoadContent();
            menu.LoadContent();

            menu.Reset();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

            background.Dispose();

            musIntro.Dispose();
            musPlay.Dispose();
            musGameOver.Dispose();

            ZeMap.UnloadContent();
            menu.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            curKeyboardState = Keyboard.GetState();
            curMouseState = Mouse.GetState();
            curGamePadState = GamePad.GetState(PlayerIndex.One);

            switch (ecran)
            {
                case Ecran.Jeu:
                    ZeMap.Update(gameTime);
                    break;
                case Ecran.Menu:
                    menu.Update(gameTime);
                    break;
            }

            oldKeyboardState = curKeyboardState;
            oldMouseState = curMouseState;
            oldGamePadState = curGamePadState;

            base.Update(gameTime);
        }
        public bool IsKeyDown(Keys key) { return curKeyboardState.IsKeyDown(key); }
        public bool IsKeyUp(Keys key) { return curKeyboardState.IsKeyUp(key); }
        public bool IsKeyPress(Keys key) { return curKeyboardState.IsKeyDown(key) && oldKeyboardState.IsKeyUp(key); }
        public bool IsKeyRelease(Keys key) { return oldKeyboardState.IsKeyDown(key) && curKeyboardState.IsKeyUp(key); }
        public Vector2 MouseLocation() { return new Vector2(curMouseState.X, curMouseState.Y); }
        public Vector2 MouseMove() { return new Vector2(curMouseState.X - oldMouseState.X, curMouseState.Y- oldMouseState.Y); }
        public enum MouseButton { Left, Middle, Right, X1, X2 }
        public bool IsMouseButtonDown(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.Left: return curMouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.Middle: return curMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.Right: return curMouseState.RightButton == ButtonState.Pressed;
                case MouseButton.X1: return curMouseState.XButton1 == ButtonState.Pressed;
                case MouseButton.X2: return curMouseState.XButton2 == ButtonState.Pressed;
                default: return false;
            }
        }
        public bool IsMouseButtonUp(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.Left: return curMouseState.LeftButton == ButtonState.Released;
                case MouseButton.Middle: return curMouseState.MiddleButton == ButtonState.Released;
                case MouseButton.Right: return curMouseState.RightButton == ButtonState.Released;
                case MouseButton.X1: return curMouseState.XButton1 == ButtonState.Released;
                case MouseButton.X2: return curMouseState.XButton2 == ButtonState.Released;
                default: return false;
            }
        }
        public bool IsMouseButtonPress(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.Left: return curMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released;
                case MouseButton.Middle: return curMouseState.MiddleButton == ButtonState.Pressed && oldMouseState.MiddleButton == ButtonState.Released;
                case MouseButton.Right: return curMouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released;
                case MouseButton.X1: return curMouseState.XButton1 == ButtonState.Pressed && oldMouseState.XButton1 == ButtonState.Released;
                case MouseButton.X2: return curMouseState.XButton2 == ButtonState.Pressed && oldMouseState.XButton2 == ButtonState.Released;
                default: return false;
            }
        }
        public bool IsMouseButtonRelease(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.Left: return curMouseState.LeftButton == ButtonState.Released && oldMouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.Middle: return curMouseState.MiddleButton == ButtonState.Released && oldMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.Right: return curMouseState.RightButton == ButtonState.Released && oldMouseState.RightButton == ButtonState.Pressed;
                case MouseButton.X1: return curMouseState.XButton1 == ButtonState.Released && oldMouseState.XButton1 == ButtonState.Pressed;
                case MouseButton.X2: return curMouseState.XButton2 == ButtonState.Released && oldMouseState.XButton2 == ButtonState.Pressed;
                default: return false;
            }
        }
        public bool IsGamePadButtonDown(Buttons button)
        {
            if (!curGamePadState.IsConnected) return false;
            return curGamePadState.IsButtonDown(button);
        }
        public bool IsGamePadButtonUp(Buttons button)
        {
            if (!curGamePadState.IsConnected) return false;
            return curGamePadState.IsButtonUp(button);
        }
        public bool IsGamePadButtonPress(Buttons button)
        {
            if (!curGamePadState.IsConnected || !oldGamePadState.IsConnected) return false;
            return curGamePadState.IsButtonDown(button) && oldGamePadState.IsButtonUp(button);
        }
        public bool IsGamePadButtonRelease(Buttons button)
        {
            if (!curGamePadState.IsConnected || !oldGamePadState.IsConnected) return false;
            return curGamePadState.IsButtonUp(button) && oldGamePadState.IsButtonDown(button);
        }
        public enum GamePadTrigger { Left, Right }
        public float GamePadTriggerValue(GamePadTrigger trigger)
        {
            if (!curGamePadState.IsConnected) return 0f;
            switch (trigger)
            {
                case GamePadTrigger.Left:
                    return curGamePadState.Triggers.Left;
                case GamePadTrigger.Right:
                    return curGamePadState.Triggers.Right;
                default: return 0f;
            }
        }
        public float GamePadTriggerDelta(GamePadTrigger trigger)
        {
            if (!curGamePadState.IsConnected || !oldGamePadState.IsConnected) return 0f;

            switch (trigger)
            {
                case GamePadTrigger.Left:
                    return curGamePadState.Triggers.Left - oldGamePadState.Triggers.Left;
                case GamePadTrigger.Right:
                    return curGamePadState.Triggers.Right - oldGamePadState.Triggers.Right;
                default: return 0f;
            }
        }
        public enum GamePadSticks { Left, Right }
        public Vector2 GamePadSticksDirection(GamePadSticks trigger)
        {
            if (!curGamePadState.IsConnected) return Vector2.Zero;
            switch (trigger)
            {
                case GamePadSticks.Left:
                    return curGamePadState.ThumbSticks.Left;
                case GamePadSticks.Right:
                    return curGamePadState.ThumbSticks.Right;
                default: return Vector2.Zero;
            }
        }
        public Vector2 GamePadSticksDelta(GamePadSticks trigger)
        {
            if (!curGamePadState.IsConnected || !oldGamePadState.IsConnected) return Vector2.Zero;
            switch (trigger)
            {
                case GamePadSticks.Left:
                    return curGamePadState.ThumbSticks.Left - oldGamePadState.ThumbSticks.Left;
                case GamePadSticks.Right:
                    return curGamePadState.ThumbSticks.Right - oldGamePadState.ThumbSticks.Right;
                default: return Vector2.Zero;
            }
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            spriteBatch.Draw(background, new Rectangle(0, 0, 1024, 768), Color.White);

            switch (ecran)
            {
                case Ecran.Jeu:
                    ZeMap.Draw(gameTime, spriteBatch); //, windowCenter);
                    break;
                case Ecran.Menu:
                    menu.Draw(gameTime, spriteBatch); 
                    break;
            }
            spriteBatch.End();
            
            base.Draw(gameTime);


        }

        public void StartNewGame()
        {
            ecran = Ecran.Jeu;
            ZeMap.Reset();
        }

        public void ReturnToMenu()
        {
            ecran = Ecran.Menu;
            menu.Reset();
        }

        public void PlayMusic(SoundEffect sound)
        {
            if (currentMusicInst != null)
            {
                currentMusicInst.Stop();
            }
            currentMusicInst = null;
            if (sound == null) return;
            currentMusicInst = sound.CreateInstance();
            currentMusicInst.IsLooped = true;
            currentMusicInst.Play();
        }

        public void PauseMusic()
        {
            currentMusicInst.Pause();
        }

        public void ResumeMusic()
        {
            currentMusicInst.Resume();
        }
    }
}
