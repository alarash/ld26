﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26
{
    class Lasers
    {
        private List<Laser> _items = new List<Laser>();
        public Laser[] items
        { get { return _items.ToArray(); } }

        private OrientedSprite sprite;

        public float vitesse = 400; // px/s

        public SoundEffect sndLaserShoot;

        public void Initialize()
        {
        }

        public void LoadContent(ContentManager Content)
        {
            sprite = OrientedSprite.Load(Content, "Objects/laser01");

            sndLaserShoot = Content.Load<SoundEffect>("sounds/snd_laser01");
        }

        public void UnloadContent()
        {
            sndLaserShoot.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            double ttm = gameTime.TotalGameTime.TotalMilliseconds;
            for (int i = 0; i < _items.Count; i++)
                if (_items[i].expireAt > 0 && _items[i].expireAt <= ttm)
                    _items.RemoveAt(i--);
                else
                {
                    _items[i].location += _items[i].sprite.direction * gameTime.ElapsedGameTime.Milliseconds * vitesse / 1000;
                    _items[i].sprite.Update(gameTime);
                }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 offset)
        {
            foreach (Laser item in items)
                item.sprite.draw(gameTime, spriteBatch, item.location + offset);
        }

        public Laser Add(Vector2 source, Vector2 desctination, double expireAt)
        {
            Laser item = new Laser() 
            { 
                location = source,
                sprite = new OrientedSprite(sprite),
                expireAt = expireAt
            };
            item.sprite.direction = desctination - source;
            item.sprite.direction.Normalize(); 

            _items.Add(item);

            return item;
        }
        public void Remove(Laser laser)
        { _items.Remove(laser); }
        public void Remove(System.Collections.Generic.ICollection<Laser> toRemove)
        { foreach (Laser item in toRemove) _items.Remove(item); }
        public void Clear()
        { _items.Clear(); }


    }
}
