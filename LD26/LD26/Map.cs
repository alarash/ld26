﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26
{
    class Map
    {
        //private AnimedSprite player;
        public Vaisseau vaisseau;
        private PowersUp powersUp;
        private Ennemis ennemies;
        private Lasers lasers;
        private double nextAllowLaser;
        public Vector2 location;
        //public MapInfo map;
        public TiledMapXml tmap;
        public Vector2 windowCenter;
        public Vector2 windowSize;

        public bool isPause;
        public Double endGameOver;
        public bool isGameOver;
        private int gameOverOffsetInit=300;
        private int gameOverOffset;

        //private Song musicInGame;
        //private Song musicGameOver;

        private AnimedSprite cursor;
        private Texture2D mask;
        private Vector2 cursorLocation;
        private SpriteFont font;

        private Dictionary<String, OrientedAnimedSprite> porteTypes = new Dictionary<string, OrientedAnimedSprite>();


        private float Vitesse = 400; // px/s 
        private int VitesseOuverturePorte = 50; // ms/frame
        private double delayLaser = 100; // ms

        private static Random rnd = new Random();

        private GameLD26 _game;

        private SoundEffect sndDoorOpen;

        public Map(GameLD26 game)
        {
            _game = game;
        }
        public void Initialize()
        {
            vaisseau = new Vaisseau();
            vaisseau.Initialize();
            powersUp = new PowersUp();
            powersUp.Initialize();
            lasers = new Lasers();
            lasers.Initialize();
            ennemies = new Ennemis();
            ennemies.Initialize();
            nextAllowLaser = 0;

            isPause = false;
			isGameOver = false;
        }

        public void LoadContent()
        {
            tmap = TiledMapXml.FromFile(_game.Content, _game.Content.RootDirectory + "/Test/map03.tmx");


            porteTypes["circle"] = OrientedAnimedSprite.Load(_game.Content, "Objects/doorCIRCLE");
            porteTypes["hexa"] = OrientedAnimedSprite.Load(_game.Content, "Objects/doorHEXA");
            porteTypes["square"] = OrientedAnimedSprite.Load(_game.Content, "Objects/doorSQUARE");
            porteTypes["triangle"] = OrientedAnimedSprite.Load(_game.Content, "Objects/doorTRIANGLE");

            vaisseau.LoadContent(_game.Content);
            powersUp.LoadContent(_game.Content);
            ennemies.LoadContent(_game.Content);
            lasers.LoadContent(_game.Content);

            cursor = AnimedSprite.Load(_game.Content, "Objects/mouseCursor01");
            mask = _game.Content.Load<Texture2D>("mask");

            font = _game.Content.Load<SpriteFont>("Test/SpriteFont1");

            sndDoorOpen      = _game.Content.Load<SoundEffect>("sounds/snd_doorOpen");

            //musicInGame = _game.Content.Load<Song>("sounds/LDMetalTest02");
            //musicGameOver = _game.Content.Load<Song>("sounds/LDGameOver");

            Reset();
        }

        public void Reset()
        {
            isPause = false;
            isGameOver = false;

            ennemies.Clear();
            powersUp.Clear();
            lasers.Clear();
            vaisseau.degat = 0;
            vaisseau.forme = "circle";

            #region Preparation des objets
 
            for (int i = 0; i < tmap.ObjectGroups[0].Objects.Length; i++)
            {
                ObjectXml obj = tmap.ObjectGroups[0].Objects[i];

                if (obj.Type == "porte")
                {
                    Vector2 dir;
                    switch (obj.Properties["orientation"])
                    {
                        case "W": dir = new Vector2(1, 0); break;
                        case "E": dir = new Vector2(-1, 0); break;
                        case "N": dir = new Vector2(0, 1); break;
                        case "S": dir = new Vector2(0, -1); break;
                        default: dir = new Vector2(1, 0); break;
                    }

                    if (obj.Properties.ContainsKey("forme"))
                        obj.tag = new OrientedAnimedSprite(porteTypes[obj.Properties["forme"]])
                        {
                            direction = dir
                        };
                    obj.Properties["etat"] = "fermee";

                }
                else if (obj.Type == "key")
                {
                    powersUp.Add(
                        obj.Properties["forme"],
                        new Vector2(obj.X + obj.Width / 2, obj.Y + obj.Height / 2),
                        0);

                    //map.objects.Remove(pair.Key); i--;
                }
                else if (obj.Type == "ennemi")
                {
                    obj.Properties["nextPop"] = "0";
                    if (!obj.Properties.ContainsKey("popSpeed"))
                        obj.Properties["popSpeed"] = "1000";
                    if (!obj.Properties.ContainsKey("popType"))
                        obj.Properties["popType"] = "type1,type2,type3";
                }
            }
            #endregion

            ObjectXml playerInfo = tmap.ObjectGroups[0].ObjectDict["player"];
            location = new Vector2(playerInfo.X + playerInfo.Width / 2f, playerInfo.Y + playerInfo.Height / 2f);
            cursorLocation = location + new Vector2(100,0);

            Vector2 realPos = cursorLocation - location + windowCenter;
            Mouse.SetPosition((int)realPos.X, (int)realPos.Y);

            

            //Mouse.SetPosition((int)location.X, (int)location.Y);
            //Mouse.SetPosition((int)cursorLocation.X, (int)cursorLocation.Y);
            //Mouse.SetPosition(10, 10);
            
            //MediaPlayer.Play(musicInGame);
            _game.PlayMusic(_game.musPlay);
        }

        public void UnloadContent()
        {
            vaisseau.UnloadContent();
            powersUp.UnloadContent();
            ennemies.UnloadContent();
            lasers.UnloadContent();

            //SOUNDS
            sndDoorOpen.Dispose();

        }

        public void Update(GameTime gameTime)
        {
            if (isGameOver)
            {
                gameOverOffset -= (int)(gameTime.ElapsedGameTime.TotalMilliseconds * 200 / 1000);
                if (gameOverOffset < 0) gameOverOffset = 0;
                if (endGameOver > gameTime.TotalGameTime.TotalMilliseconds) return;
                _game.ReturnToMenu();
                return;
            }
            if (_game.IsKeyPress(Keys.P) || _game.IsGamePadButtonPress(Buttons.Start))
            {
                isPause ^= true;
                if (isPause)
                {
                    //MediaPlayer.Pause();
                    _game.PauseMusic();
                }
                else
                {
                    //MediaPlayer.Resume();
                    _game.ResumeMusic();
                }
            }
            if (_game.IsKeyPress(Keys.Escape))
            {
                _game.ReturnToMenu();
                return;
            }


            if (isPause) return;

            Vector2 dir = _game.GamePadSticksDirection(GameLD26.GamePadSticks.Left);
            dir.Y *= -1;
            if (dir == Vector2.Zero)
            {
                if (_game.IsKeyDown(Keys.Left) || _game.IsKeyDown(Keys.Q) || _game.IsKeyDown(Keys.A)) dir.X--;
                if (_game.IsKeyDown(Keys.Right) || _game.IsKeyDown(Keys.D)) dir.X++;
                if (_game.IsKeyDown(Keys.Up) || _game.IsKeyDown(Keys.Z) || _game.IsKeyDown(Keys.W)) dir.Y--;
                if (_game.IsKeyDown(Keys.Down) || _game.IsKeyDown(Keys.S)) dir.Y++;
            }
            
            #region DebugZone
            //if (_game.IsKeyPress(Keys.R)) Reset();
            /*
            if (_game.IsKeyPress(Keys.A)) vaisseau.forme = "circle";
            if (_game.IsKeyPress(Keys.Z)) vaisseau.forme = "hexa";
            if (_game.IsKeyPress(Keys.E)) vaisseau.forme = "square";
            if (_game.IsKeyPress(Keys.R)) vaisseau.forme = "triangle";
            */

            /*
            if (_game.IsKeyPress(Keys.Q))
                powersUp.Add("circle", new Vector2(location.X - 30f, location.Y - 30f), gameTime.TotalGameTime.TotalMilliseconds + 5000);
            if (_game.IsKeyPress(Keys.S))
                powersUp.Add("hexa", new Vector2(location.X - 30f, location.Y - 30f), gameTime.TotalGameTime.TotalMilliseconds + 5000);
            if (_game.IsKeyPress(Keys.D))
                powersUp.Add("square", new Vector2(location.X - 30f, location.Y - 30f), gameTime.TotalGameTime.TotalMilliseconds + 5000);
            if (_game.IsKeyPress(Keys.F))
                powersUp.Add("triangle", new Vector2(location.X - 30f, location.Y - 30f), gameTime.TotalGameTime.TotalMilliseconds + 5000);
            /**/
            /*
            if (_game.IsKeyPress(Keys.W))
                ennemies.Add("type1", new Vector2(location.X - 30f, location.Y - 30f));
            if (_game.IsKeyPress(Keys.X))
                ennemies.Add("type2", new Vector2(location.X - 30f, location.Y - 30f));
            if (_game.IsKeyPress(Keys.C))
                ennemies.Add("type3", new Vector2(location.X - 30f, location.Y - 30f));
            if (_game.IsKeyPress(Keys.V))
                ennemies.Add("type1,type2,type3", new Vector2(location.X - 30f, location.Y - 30f));
            /**/
            #endregion

            powersUp.Update(gameTime); // Refresh Sprite

            #region Vaisseau Move and Hit Door/Wall
            if (dir.LengthSquared() != 0)
            {
                dir.Normalize();
                dir *= gameTime.ElapsedGameTime.Milliseconds * Vitesse / 1000f;

                vaisseauHitDoor(ref dir);
                vaisseauHitMap(ref dir);

                location += dir;
            }
            #endregion

            vaisseauHitPowerUp();

            ennemies.Update(gameTime, this); // IA Move and Refresh Sprite
            popEnnemis(gameTime);
            vaisseauHitEnnemie(gameTime);
            
            #region Gestion du cursor
            cursor.Update(gameTime); // Refresh Sprite
            Vector2 cursorDir = _game.GamePadSticksDirection(GameLD26.GamePadSticks.Right);
            cursorDir.Y *= -1;

            if (cursorDir != Vector2.Zero)
            {
                if (cursorDir.LengthSquared() > 1) cursorDir.Normalize();
                cursorLocation += cursorDir * (float)(gameTime.ElapsedGameTime.TotalMilliseconds * 1000 / 1000)
                    + dir;
            }
            else if (_game.MouseMove() != Vector2.Zero)
                cursorLocation = _game.MouseLocation() + location - windowCenter;
            else
                cursorLocation += dir;
            #endregion

            #region Send Laser
            //if (_game.IsKeyPress(Keys.Space) ||_game.IsMouseButtonPress(GameLD26.MouseButton.Left))
            if (nextAllowLaser <= gameTime.TotalGameTime.TotalMilliseconds &&
                (_game.IsKeyDown(Keys.Space) || _game.IsMouseButtonDown(GameLD26.MouseButton.Left) ||
                _game.GamePadTriggerValue(GameLD26.GamePadTrigger.Left)>0.2f ||
                _game.GamePadTriggerValue(GameLD26.GamePadTrigger.Right)>0.2f))
                sendNextLaser(gameTime);
            #endregion

            lasers.Update(gameTime); // Refresh location and sprite

            laserHitEnnemie(gameTime);
            laserHitWall();
            laserHitDoor();

            #region Refresh door sprite
            foreach(ObjectXml obj in tmap.ObjectGroups[0].Objects)
            {
                if (obj.Type == "porte" && obj.tag is OrientedAnimedSprite)
                {
                    OrientedAnimedSprite porte = (OrientedAnimedSprite)obj.tag;
                    porte.Update(gameTime);
                    if (obj.Properties["etat"] == "ouverture" && porte.nbLoop > 0)
                    {
                        porte.stepTime = 0;
                        porte.currentStep = porte.nbStep - 1;
                        obj.Properties["etat"] = "ouvert";
                        porte.Update(gameTime);
                    }
                }
            }
            #endregion

            vaisseau.Update(gameTime);
        }

        public void sendNextLaser(GameTime gameTime)
        {
            Laser main = lasers.Add(location, cursorLocation, gameTime.TotalGameTime.TotalMilliseconds + 3000);
            Vector2 mainDir = main.sprite.direction;

            int nbLaser=0;
            int start = -1;
            int pas = 1;
            double r = Math.PI * 2 / nbLaser; //0.1f;
            if (vaisseau.forme == "circle")
            {
                nbLaser = 1;
            }
            else if (vaisseau.forme == "triangle")
            {
                nbLaser = 3;
                start = 1;
                pas = 2;
                r = Math.PI  / nbLaser;
            }
            else if (vaisseau.forme == "square")
            {
                nbLaser = 4;
                start = 1;
                pas = 1;
                r = Math.PI * 2 / nbLaser;
            }
            else if (vaisseau.forme == "hexa")
            {
                nbLaser = 6;
                start = 2;
                pas = 1;
                r = Math.PI * 2 / nbLaser;
            }

            for (int i = -start; i <= start; i += pas)
            {
                double r2 = Math.PI - r * i;

                lasers.Add(
                    location,
                    location + new Vector2(
                        (float)(main.sprite.direction.X * Math.Cos(r2) + main.sprite.direction.Y * Math.Sin(r2)),
                        (float)(main.sprite.direction.Y * Math.Cos(r2) - main.sprite.direction.X * Math.Sin(r2))),
                    gameTime.TotalGameTime.TotalMilliseconds + 3000);
            }
            nextAllowLaser = gameTime.TotalGameTime.TotalMilliseconds + delayLaser*nbLaser;

            lasers.sndLaserShoot.Play();
        }
        public void vaisseauHitPowerUp()
        {
            Rectangle hit = vaisseau.hit;
            hit.X += (int)(location.X - vaisseau.centre.X);
            hit.Y += (int)(location.Y  - vaisseau.centre.Y);

            List<PowerUp> powerUpHited = powersUp.hit(hit);
            if (powerUpHited.Count == 0) return;

            if (vaisseau.forme != powerUpHited[0].forme)
                powersUp.sndPickUp.Play();

            vaisseau.forme = powerUpHited[0].forme;

            for (int i = 0; i < powerUpHited.Count; i++)
                if (powerUpHited[i].expireAt == 0)
                    powerUpHited.RemoveAt(i--);

            powersUp.Remove(powerUpHited);
        }
        public void vaisseauHitDoor(ref Vector2 dir)
        {
            Rectangle hit = vaisseau.hit;

            hit.X += (int)(location.X + dir.X - vaisseau.centre.X);
            hit.Y += (int)(location.Y + dir.Y - vaisseau.centre.Y);

            foreach(ObjectXml obj in tmap.ObjectGroups[0].Objects)
                if (obj.Type == "porte")
                {
                    OrientedAnimedSprite porte = (OrientedAnimedSprite)obj.tag;

                    if (obj.Properties["etat"] != "ouvert")
                    {
                        if (hit.Intersects(new Rectangle(obj.X, obj.Y, obj.Width, obj.Height)))
                        {
                            if (vaisseau.forme == obj.Properties["forme"] && 
                                obj.Properties["etat"] != "ouverture")
                            {
                                obj.Properties["etat"] = "ouverture";
                                porte.stepTime = VitesseOuverturePorte;
                                porte.timeElapsed = 0;
                                porte.currentStep = 0;

                                sndDoorOpen.Play();
                            }
                            dir = new Vector2(); 
                        }
                    }
                }
        }
        public void vaisseauHitMap(ref Vector2 dir)
        {
            if (dir.X < 0)
            {
                int[] hitsIndex = getHitsIndex(vaisseau.hit, location + new Vector2(dir.X, 0), vaisseau.centre);
                if (tmap.getTileAt(hitsIndex[0]) != 0 || tmap.getTileAt(hitsIndex[2]) != 0)
                    dir.X = tmap.getTilePos(hitsIndex[0]).X + tmap.TileWidth
                        - location.X + vaisseau.centre.X - vaisseau.hit.X;
            }
            if (dir.X > 0)
            {
                int[] hitsIndex = getHitsIndex(vaisseau.hit, location + new Vector2(dir.X, 0), vaisseau.centre);
                if (tmap.getTileAt(hitsIndex[1]) != 0 || tmap.getTileAt(hitsIndex[3]) != 0)
                    dir.X = tmap.getTilePos(hitsIndex[1]).X - location.X
                        + vaisseau.centre.X - 1
                        - vaisseau.hit.Width - vaisseau.hit.X;
            }
            if (dir.Y < 0)
            {
                int[] hitsIndex = getHitsIndex(vaisseau.hit, location + new Vector2(0, dir.Y), vaisseau.centre);
                if (tmap.getTileAt(hitsIndex[0]) != 0 || tmap.getTileAt(hitsIndex[1]) != 0)
                    dir.Y = tmap.getTilePos(hitsIndex[0]).Y + tmap.TileHeight
                        - location.Y + vaisseau.centre.Y - vaisseau.hit.Y;
            }
            if (dir.Y > 0)
            {
                int[] hitsIndex = getHitsIndex(vaisseau.hit, location + new Vector2(0, dir.Y), vaisseau.centre);
                if (tmap.getTileAt(hitsIndex[2]) != 0 || tmap.getTileAt(hitsIndex[3]) != 0)
                    dir.Y = tmap.getTilePos(hitsIndex[2]).Y - location.Y
                        + vaisseau.centre.Y - 1
                        - vaisseau.hit.Height - vaisseau.hit.Y;
            }
        }
        public void laserHitEnnemie(GameTime gameTime)
        {
            foreach (EnnemiBase ennemi in ennemies.items)
                if (ennemi.etat == EnnemiBase.Etat.normal)
                {
                    Rectangle hit = ennemi.spriteEnnemie.hit;
                    hit.X += (int)(ennemi.location.X - ennemi.spriteEnnemie.centre.X);
                    hit.Y += (int)(ennemi.location.Y - ennemi.spriteEnnemie.centre.Y);

                    foreach (Laser laser in lasers.items)
                    {
                        if (hit.Contains((int)laser.location.X, (int)laser.location.Y))
                        {
                            lasers.Remove(laser);
                            ennemi.etat = EnnemiBase.Etat.explotion;
                            nextAllowLaser -= delayLaser;

                            if (rnd.Next(100)<5)
                            {
                                String[] formes = new String[] { "circle", "triangle", "square", "hexa" };
                                powersUp.Add(formes [rnd.Next(formes .Length)], ennemi.location, 
                                    gameTime.TotalGameTime.TotalMilliseconds + 5000);
                            }

                            ennemies.sndExplosion.Play();
                            break;
                        }
                    }
                }
        }
        public void laserHitWall()
        {
            foreach (Laser laser in lasers.items)
                if (tmap.getTileAt(laser.location) != 0)
                {
                    lasers.Remove(laser);
                    nextAllowLaser -= delayLaser/2;
                }
        }
        public void laserHitDoor()
        {
            foreach (Laser laser in lasers.items)
                //foreach (MapInfo.MapObject obj in map.objects.Values)
                foreach(ObjectXml obj in tmap.ObjectGroups[0].Objects)
                    if (obj.Type == "porte" && obj.Properties["etat"] != "ouvert")
                    {
                        Rectangle hit = new Rectangle(obj.X, obj.Y, obj.Width, obj.Height);
                        if (hit.Contains((int)laser.location.X, (int)laser.location.Y))
                        {
                            lasers.Remove(laser);
                            nextAllowLaser -= delayLaser / 2;
                        }
                    }
        }
        public void vaisseauHitEnnemie(GameTime gameTime)
        {
            Rectangle hitVaisseau = vaisseau.hit;
            hitVaisseau.X += (int)(location.X - vaisseau.centre.X);
            hitVaisseau.Y += (int)(location.Y - vaisseau.centre.Y);

            foreach (EnnemiBase ennemi in ennemies.items)
                if (ennemi.etat == EnnemiBase.Etat.normal)
                {
                    Rectangle hit = ennemi.spriteEnnemie.hit;
                    hit.X += (int)(ennemi.location.X - ennemi.spriteEnnemie.centre.X);
                    hit.Y += (int)(ennemi.location.Y - ennemi.spriteEnnemie.centre.Y);

                    if (hitVaisseau.Intersects(hit))
                    {
                        vaisseau.degat++;
                        ennemi.etat = EnnemiBase.Etat.explotion;
                        ennemies.sndExplosion.Play();

                        if (vaisseau.degat >= 100)
                        {
                            isGameOver = true;
                            endGameOver = gameTime.TotalGameTime.TotalMilliseconds + 5000;
                            gameOverOffset = gameOverOffsetInit;
                            //MediaPlayer.Play(musicGameOver);

                            _game.PlayMusic(_game.musGameOver);
                        }
                    }
                }
        }

        public void popEnnemis(GameTime gameTime)
        {
            //if (nextPop > gameTime.TotalGameTime.TotalMilliseconds) return;

            //foreach (MapInfo.MapObject obj in map.objects.Values)
            foreach(ObjectXml obj in tmap.ObjectGroups[0].Objects)
                if (obj.Type == "ennemi")
                {
                    Vector2 distance = location - new Vector2(obj.X, obj.Y) - new Vector2(obj.Width, obj.Height) / 2;
                    if (distance.LengthSquared() <= windowCenter.LengthSquared())
                    {
                        if (Double.Parse(obj.Properties["nextPop"]) < gameTime.TotalGameTime.TotalMilliseconds)
                        {
                            Vector2 eLoc = new Vector2(rnd.Next(obj.Width) + obj.X, rnd.Next(obj.Height) + obj.Y);
                            if ((eLoc - location).Length() > 150)
                                ennemies.Add(obj.Properties["popType"], eLoc);

                            int popSpeed = int.Parse(obj.Properties["popSpeed"]);
                            double nextPop = gameTime.TotalGameTime.TotalMilliseconds + popSpeed*rnd.Next(80,120)/100;
                            obj.Properties["nextPop"] = nextPop.ToString();
                        }
                    }
                }

        }


        public int[] getHitsIndex(Rectangle hit, Vector2 location, Vector2 offset)
        {
            Vector2 corner1 = new Vector2(hit.Left, hit.Top) + location - offset;
            Vector2 corner2 = new Vector2(corner1.X + hit.Width, corner1.Y);
            Vector2 corner3 = new Vector2(corner1.X, corner1.Y + hit.Height);
            Vector2 corner4 = new Vector2(corner2.X, corner3.Y);

            return new int[] { 
                tmap.getTileIndexAt(corner1), 
                tmap.getTileIndexAt(corner2), 
                tmap.getTileIndexAt(corner3), 
                tmap.getTileIndexAt(corner4)};
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) //, Vector2 windowCenter)
        {
            lasers.Draw(gameTime, spriteBatch, windowCenter - location);

            tmap.Draw(gameTime, spriteBatch, location - windowCenter);
            //map.draw(gameTime, spriteBatch, location - windowCenter);

            powersUp.Draw(gameTime, spriteBatch, windowCenter - location);

            //foreach (MapInfo.MapObject obj in map.objects.Values)
            foreach(ObjectXml obj in tmap.ObjectGroups[0].Objects)
            {
                if (obj.Type == "porte" && obj.tag is OrientedAnimedSprite)
                {
                    OrientedAnimedSprite porte = (OrientedAnimedSprite)obj.tag;
                    porte.draw(
                        gameTime, spriteBatch,
                        new Vector2(obj.X + obj.Width / 2, obj.Y + obj.Height / 2) 
                        + windowCenter - location + porte.centre);
                    //map.tile.draw(gameTime, spriteBatch, new Vector2(obj.x, obj.y) - location + windowCenter, 1);
                }
            }

            ennemies.Draw(gameTime, spriteBatch, windowCenter - location);
            cursor.draw(gameTime, spriteBatch, cursorLocation - location + windowCenter);
            vaisseau.Draw(gameTime, spriteBatch, windowCenter);

            //spriteBatch.DrawString(font, "Degat : " + vaisseau.degat.ToString() + "/100", 
            //    new Vector2(10, 10), new Color(1f, 0f, 0f));
            spriteBatch.DrawString(font, "HEALTH : " + (100 - vaisseau.degat).ToString() + "/100", 
                new Vector2(10, 10), new Color(1f, 1f, 1f));

            if (isGameOver)
            {
                spriteBatch.Draw(mask,
                    new Rectangle(0, 0, (int)windowSize.X, (int)windowSize.Y),
                    new Color(0f, 0f, 0f, 0.8f * (gameOverOffsetInit-gameOverOffset) / gameOverOffsetInit));

                spriteBatch.DrawString(font, "GAME\nOVER", new Vector2(windowCenter.X, windowCenter.Y-gameOverOffset),
                    new Color(0.8f, 0f, 0f, 1f*gameOverOffset / gameOverOffsetInit), 0,
                    font.MeasureString("GAME\nOVER") / 2, new Vector2(5f), SpriteEffects.None, 0f);
            }
            else if (isPause)
            {
                spriteBatch.Draw(mask, 
                    new Rectangle(0, 0, (int)windowSize.X, (int)windowSize.Y),
                    new Color(0f, 0f, 0f, 0.8f));

                spriteBatch.DrawString(font, "PAUSE", windowCenter, new Color(0.8f, 0f, 0f), 0,
                    font.MeasureString("PAUSE")/2, new Vector2(5f), SpriteEffects.None, 0f);
                ;

            }

        }

    }
}
