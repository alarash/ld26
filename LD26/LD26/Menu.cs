﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26
{
    class Menu
    {
        private int selectedItem=0;

        private Texture2D background;

        private String[] menuItems = new String[] { "Start", "Quit" };
        private SpriteFont font;
        public Vector2 windowCenter;
        public Vector2 windowSize;
        
        //private Song music;
        //private SoundEffect music;
        //private SoundEffectInstance musicInst;

        private GameLD26 _game;

        public Menu(GameLD26 game)
        {
            _game = game;
        }

        public void Initialize()
        {
        }

        public void LoadContent()
        {
            font = _game.Content.Load<SpriteFont>("Test/SpriteFont1");
            //music = _game.Content.Load<Song>("sounds/LDIntro");
            //music = _game.Content.Load<SoundEffect>("sounds/LDIntro");
            //musicInst = music.CreateInstance();
            //musicInst.IsLooped = true;

            background = _game.Content.Load<Texture2D>("title");
        }
        public void UnloadContent()
        {
            background.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed) _game.Exit();
            if (_game.IsKeyPress(Keys.Down) || _game.IsKeyPress(Keys.S) ||
                _game.IsGamePadButtonPress(Buttons.DPadUp) || 
                _game.IsGamePadButtonPress(Buttons.LeftThumbstickDown) ||
                _game.IsGamePadButtonPress(Buttons.RightThumbstickDown) ||
                _game.GamePadSticksDirection(GameLD26.GamePadSticks.Left).Y < 0 ||
                _game.GamePadSticksDirection(GameLD26.GamePadSticks.Right).Y < 0)
                selectedItem++;
            if (_game.IsKeyPress(Keys.Up) || _game.IsKeyPress(Keys.Z) || _game.IsKeyPress(Keys.W) ||
                _game.IsGamePadButtonPress(Buttons.DPadDown) || 
                _game.IsGamePadButtonPress(Buttons.LeftThumbstickUp) ||
                _game.IsGamePadButtonPress(Buttons.RightThumbstickUp) ||
                _game.GamePadSticksDirection(GameLD26.GamePadSticks.Left).Y > 0 ||
                _game.GamePadSticksDirection(GameLD26.GamePadSticks.Right).Y > 0)
                selectedItem--;
            if (selectedItem < 0) selectedItem = 0;
            if (selectedItem >= menuItems.Length) selectedItem = menuItems.Length - 1;

            if (_game.IsKeyPress(Keys.Enter) || _game.IsKeyPress(Keys.Space) || _game.IsKeyPress(Keys.Right) ||
                _game.IsGamePadButtonPress(Buttons.A) || _game.IsGamePadButtonPress(Buttons.B) ||
                _game.IsGamePadButtonPress(Buttons.X) || _game.IsGamePadButtonPress(Buttons.Y) ||
                _game.IsGamePadButtonPress(Buttons.Start))
                execMenu();
        }
        public void execMenu()
        {
            switch (selectedItem)
            {
                case 0: _game.StartNewGame(); break;
                case 1: _game.Exit(); break;
            }
        }

        public void Reset()
        {
            selectedItem = 0;
            //MediaPlayer.Play(music);
            //musicInst.Play();
            _game.PlayMusic(_game.musIntro);
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) //, Vector2 windowCenter)
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, 1024, 768), Color.White);
            
            /*String titre;
            titre = "Ludum Dare 26";
            spriteBatch.DrawString(font, titre, new Vector2(windowCenter.X, windowCenter.Y / 2-40), new Color(1.0f, 0f, 0f), 0,
                font.MeasureString(titre) / 2, new Vector2(3.5f), SpriteEffects.None, 0f);
            titre = "Minimalism";
            spriteBatch.DrawString(font, titre, new Vector2(windowCenter.X, windowCenter.Y / 2+30), new Color(1.0f, 0f, 0f), 0,
                font.MeasureString(titre) / 2, new Vector2(3.5f), SpriteEffects.None, 0f);*/

            int lineSpace = 60;
            Vector2 textOffset = new Vector2(windowCenter.X, windowCenter.Y + 150 - lineSpace * menuItems.Length / 2);
            for (int i = 0; i < menuItems.Length; i++)
            {
                String t = menuItems[i];
                if (selectedItem == i) t = "-> " + t + " <-";

                spriteBatch.DrawString(font, t, textOffset, new Color(1f, 1f, 1f), 0,
                    font.MeasureString(t) / 2, new Vector2(2f), SpriteEffects.None, 0f);

                textOffset.Y += lineSpace;
            }

            /*String text = "By StabAlarash & Nathan";
            spriteBatch.DrawString(font, text, new Vector2(0, windowSize.Y - 30), new Color(1f, 1f, 1f), 0,
                Vector2.Zero, new Vector2(1f), SpriteEffects.None, 0f);*/
        }

    }
}
