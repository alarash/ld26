﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26
{
    class PowersUp
    {
        private Dictionary<String, AnimedSprite> types = new Dictionary<String, AnimedSprite>();
        private List<PowerUp> _items = new List<PowerUp>();
        public PowerUp[] items
        { get { return _items.ToArray(); } }

        public SoundEffect sndPickUp;
        
        public void Initialize()
        {
        }

        public void LoadContent(ContentManager Content)
        {
            types["circle"] = AnimedSprite.Load(Content, "Objects/keyCIRCLE");
            types["hexa"] = AnimedSprite.Load(Content, "Objects/keyHEXA");
            types["square"] = AnimedSprite.Load(Content, "Objects/keySQUARE");
            types["triangle"] = AnimedSprite.Load(Content, "Objects/keyTRIANGLE");

            sndPickUp = Content.Load<SoundEffect>("sounds/snd_pickup");
        }

        public void UnloadContent()
        {
            sndPickUp.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            double ttm = gameTime.TotalGameTime.TotalMilliseconds;
            for (int i = 0; i < _items.Count; i++)
                if (_items[i].expireAt>0 && _items[i].expireAt <= ttm)
                    _items.RemoveAt(i--);
                else
                    _items[i].sprite.Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 offset)
        {
            foreach (PowerUp item in _items)
                item.sprite.draw(gameTime, spriteBatch, item.location + offset);
        }

        public void Add(String forme, Vector2 location, double expireAt)
        {
            _items.Add(new PowerUp() 
            { 
                forme = forme, 
                location = location, 
                sprite = types[forme],
                expireAt = expireAt
            });
        }
        public void Remove(PowerUp powerUp) 
        { _items.Remove(powerUp); }
        public void Remove(System.Collections.Generic.ICollection<PowerUp> toRemove)
        { foreach (PowerUp item in toRemove) _items.Remove(item); }
        public void Clear()
        { _items.Clear(); }

        public List<PowerUp> hit(Rectangle hitBox)
        {
            List<PowerUp> ret = new List<PowerUp>();
            foreach (PowerUp item in _items)
            {
                Rectangle spHit = item.sprite.hit;
                spHit.X += (int)(item.location.X - item.sprite.centre.X);
                spHit.Y += (int)(item.location.Y - item.sprite.centre.Y);

                if (hitBox.Intersects(spHit))
                    ret.Add(item);
            }
            return ret;
        }
    }
}
