﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LD26Lib;

namespace LD26
{
    class Vaisseau
    {
        private int _degat = 0;
        public int degat
        {
            get { return _degat; }
            set
            {
                _degat = value;
                System.Diagnostics.Debug.WriteLine("Degat : " + _degat.ToString());
            }
        }

        public void Initialize()
        {
        }

        private Dictionary<String, AnimedSprite> types = new Dictionary<string, AnimedSprite>();
        private String _forme;
        public String forme
        { 
            get { return _forme; } 
            set 
            { 
                _forme = value; 
                _currentSprite = types[_forme]; 
            } 
        }

        private AnimedSprite _currentSprite;
        public AnimedSprite getCurrentSprite()
        { return _currentSprite; }

        public Rectangle hit
        { get { return _currentSprite.hit; } }
        public Vector2 centre
        { get { return _currentSprite.centre; } }


        public void LoadContent(ContentManager Content)
        {
            types["circle"] = AnimedSprite.Load(Content, "Objects/shapeCIRCLE");
            types["hexa"] = AnimedSprite.Load(Content, "Objects/shapeHEXA");
            types["square"] = AnimedSprite.Load(Content, "Objects/shapeSQUARE");
            types["triangle"] = AnimedSprite.Load(Content, "Objects/shapeTRIANGLE");

            forme = "circle";
        }

        public void UnloadContent()
        {
        }

        public void Update(GameTime gameTime)
        {
            _currentSprite.Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 location)
        {
            _currentSprite.draw(gameTime, spriteBatch, location);
        }

    }
}
