using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD26Lib
{
    public class AnimedSprite : Sprite
    {
        public Vector2 Size;
        public int nbStep;
        public int stepTime;
        public String spriteName;
        public Vector2 centre;
        public Rectangle hit;

        protected Rectangle _clip;

        private int _nbLoop = 0;
        public int nbLoop { get { return _nbLoop; } }

        [ContentSerializerIgnore]
        public double timeElapsed = 0;
        [ContentSerializerIgnore]
        public int currentStep = 0;

        protected AnimedSprite() { }
        public AnimedSprite(AnimedSprite src)
        {
            Size = src.Size;
            hit = src.hit;
            nbStep = src.nbStep;
            stepTime = src.stepTime;
            spriteName = src.spriteName;
            centre = src.centre;
            _sprite = src._sprite;
            _clip = src._clip;
        }

        public virtual void Initialize(ContentManager Content)
        {
            _sprite = Content.Load<Texture2D>(spriteName);
            _clip = new Rectangle(0, 0, (int)Size.X, (int)Size.Y);
        }
        public new static AnimedSprite Load(ContentManager Content, String asset)
        {
            AnimedSprite anim = Content.Load<AnimedSprite>(asset);
            anim.Initialize(Content);
            return anim;
        }

        public override void Update(GameTime gameTime)
        {
            timeElapsed += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (stepTime != 0) while (timeElapsed >= stepTime)
            {
                timeElapsed -= stepTime;
                currentStep++;
                if (currentStep >= nbStep) { currentStep = 0; _nbLoop++; }
            }
            _clip.X = (int)(Size.X * currentStep);
        }
        public override void draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            Vector2 pos = new Vector2((int)(position.X - centre.X), (int)(position.Y - centre.Y));
            //spriteBatch.Draw(_sprite, position - centre, _clip, Color.White, 10f, centre, 1f, SpriteEffects.None ,0f);
            //spriteBatch.Draw(_sprite, position - centre, _clip, Color.White);
            spriteBatch.Draw(_sprite, pos, _clip, Color.White);
        }
    }
}
