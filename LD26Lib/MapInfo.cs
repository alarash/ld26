﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD26Lib
{
    
    public class MapInfo
    {
        public String tileName;
        public int width;
        public int height;

        public int[] datas;

        public Dictionary<String, MapObject> objects;
        public class MapObject
        {
            [ContentSerializerIgnore]
            public Object tag;

            public String type;
            public int width;
            public int height;
            public int x;
            public int y;
            public Dictionary<String, String> properties;
        }
    
        private TileSprite _tile;
        [ContentSerializerIgnore]
        public TileSprite tile
        { get { return _tile; } }


        protected MapInfo() { }
        public MapInfo(MapInfo src)
        {
            tileName = src.tileName;
            width = src.width;
            height = src.height;
            datas = (int[])src.datas.Clone();
            _tile = src._tile;
        }

        public static MapInfo Load(ContentManager Content, String asset)
        {
            MapInfo mapInfo = Content.Load<MapInfo>(asset);
            //mapInfo._tile = Content.Load<TileSprite>(mapInfo.tileName);
            mapInfo._tile = TileSprite.Load(Content, mapInfo.tileName);
            
            return mapInfo;
        }

        public void draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            //GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            Vector2 pos = new Vector2();
            for (int x = 0; x<width; x++)
                for (int y = 0; y < height; y++)
                {
                    int i = y * width + x;
                    pos.X = _tile.Size.X * x;
                    pos.Y = _tile.Size.Y * y;
                    //    if (datas[i]==1)
                    _tile.draw(gameTime, spriteBatch, pos - position, datas[i]);
                }
        }

        public int getTileIndexAt(Vector2 pos)
        {
            int X = (int)(pos.X / _tile.Size.X);
            int Y = (int)(pos.Y / _tile.Size.Y);
            if (X < 0 || Y < 0 || X >= width  || Y >= height) return -1;
            return Y * width + X;
        }
        public int getTileAt(Vector2 pos)
        {
            return getTileAt(getTileIndexAt(pos));
        }
        public int getTileAt(int index)
        {
            if (index < 0) return -1;
            return datas[index];
        }
        public Vector2 getTilePos(int index)
        {
            return new Vector2((index % width) * _tile.Size.X, (index / width) * _tile.Size.Y);
        }
    }
}
