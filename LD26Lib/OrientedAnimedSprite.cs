using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD26Lib
{
    public class OrientedAnimedSprite : AnimedSprite
    {
        public Vector2 direction;


        protected OrientedAnimedSprite() { }
        public OrientedAnimedSprite(OrientedAnimedSprite src):base(src)
        {
            direction = src.direction;
        }

        public override void Initialize(ContentManager Content)
        {
            base.Initialize(Content);

        }
        public new static OrientedAnimedSprite Load(ContentManager Content, String asset)
        {
            OrientedAnimedSprite anim = Content.Load<OrientedAnimedSprite>(asset);
            anim.Initialize(Content);
            return anim;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        public override void draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            Vector2 pos = new Vector2((int)(position.X - centre.X), (int)(position.Y - centre.Y));
            if (direction.LengthSquared() == 0f) direction = new Vector2(1, 0);
            direction.Normalize();
            float rot;
            if (direction.Y!=0)
                rot = (float)(Math.Acos(direction.X) * Math.Sign(Math.Asin(direction.Y)));
            else
                rot = (float)Math.Acos(direction.X);
            spriteBatch.Draw(_sprite, pos, _clip, Color.White, rot, centre, 1f, SpriteEffects.None, 0);
            //spriteBatch.Draw(_sprite, pos, _clip, Color.White);
        }
    }
}
