﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD26Lib
{
    public class OrientedSprite : Sprite
    {
        //public Vector2 size;
        public Vector2 direction;
        public String spriteName;
        public Vector2 centre;
        public Rectangle hit;

        protected OrientedSprite() { }
        public OrientedSprite(OrientedSprite src)
        {
            //size = src.size;
            hit = src.hit;
            spriteName = src.spriteName;
            centre = src.centre;
            _sprite = src._sprite;
            direction = src.direction;
        }

        public new static OrientedSprite Load(ContentManager Content, String asset)
        {
            OrientedSprite anim = Content.Load<OrientedSprite>(asset);
            anim._sprite = Content.Load<Texture2D>(anim.spriteName);
            return anim;
        }
        /*
        public override void Update(GameTime gameTime)
        {
        }
        */

        public override void draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            Vector2 pos = new Vector2((int)(position.X), (int)(position.Y));
            if (direction.LengthSquared() == 0f) direction = new Vector2(1, 0);
            direction.Normalize();
            float rot =(float)(Math.Acos(direction.X) * Math.Sign(Math.Asin(direction.Y)));
            //spriteBatch.Draw(_sprite, pos, null, Color.White, rot, centre, 1f, SpriteEffects.None, 0);
            spriteBatch.Draw(_sprite, pos, null, Color.White, rot, centre, 1f, SpriteEffects.None, 0);
            //spriteBatch.Draw(_sprite, pos, null, Color.White);
        }

    }
}
