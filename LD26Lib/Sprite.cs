using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD26Lib
{
    public class Sprite
    {
        protected Texture2D _sprite;

        protected Sprite() { }

        public static Sprite Load(ContentManager Content, String asset)
        {
            Sprite sprite = new Sprite();
            sprite._sprite = Content.Load<Texture2D>(asset);
            return sprite;
        }

        public virtual void Update(GameTime gameTime) { }
        public virtual void draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            spriteBatch.Draw(_sprite, position, Color.White);
        }
    }
}
