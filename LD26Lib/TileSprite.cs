﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LD26Lib
{
    public class TileSprite : Sprite
    {
        public Vector2 Size;
        public String spriteName;
        public int nbByLine;
        public int firstGid;

        private Vector2 _centre;
        //public Vector2 centre { get { return _centre; } }
        private Rectangle _clip;

        protected TileSprite() { }
        public TileSprite(TileSprite src)
        {
            Size = src.Size;
            spriteName = src.spriteName;
            _sprite = src._sprite;
            _centre = src._centre;
        }

        public new static TileSprite Load(ContentManager Content, String asset)
        {
            TileSprite tile = Content.Load<TileSprite>(asset);
            tile._sprite = Content.Load<Texture2D>(tile.spriteName);
            tile._clip = new Rectangle(0, 0, (int)tile.Size.X, (int)tile.Size.Y);
            tile._centre = new Vector2(tile.Size.X / 2, tile.Size.Y / 2); 
            return tile;
        }

        private int _lastIndex;
        public override void draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        { draw(gameTime, spriteBatch, position, _lastIndex); }
        public void draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position, int index)
        {
            _lastIndex = index;
            /*
            _clip.X = _clip.Width * ((index - firstGid) % nbByLine);
            _clip.Y = _clip.Height * ((index - firstGid) / nbByLine);
            */
            if (index < firstGid) return;
            _clip.X = _clip.Width * ((index - firstGid) % nbByLine);
            _clip.Y = _clip.Height * ((index - firstGid) / nbByLine);
            Vector2 pos = position; // -_centre;
            pos.X = (int)pos.X;
            pos.Y = (int)pos.Y;
            spriteBatch.Draw(_sprite, pos, _clip, Color.White);
        }


    }
}
