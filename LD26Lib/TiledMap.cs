﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace LD26Lib
{
    [XmlRoot("map", IsNullable = false)]
    public class TiledMapXml
    {
        public static TiledMapXml FromFile(ContentManager Content, String file)
        {
            using (var stream = File.OpenRead(file))
            {
                return FromStream(Content, stream);
            }
        }

        public static TiledMapXml FromStream(ContentManager Content, Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TiledMapXml));
            TiledMapXml tm = (TiledMapXml)serializer.Deserialize(stream);

            tm.PostProcess(Content);

            return tm;
        }

        [XmlAttribute("width")]
        public Int32 Width;

        [XmlAttribute("height")]
        public Int32 Height;

        [XmlAttribute("tilewidth")]
        public Int32 TileWidth;

        [XmlAttribute("tileheight")]
        public Int32 TileHeight;

        [XmlElement("tileset")]
        public TilesetXml[] Tilesets;

        [XmlElement("layer")]
        public LayerXml[] Layers;

        [XmlElement("objectgroup")]
        public ObjectGroupXml[] ObjectGroups;


        [XmlIgnore]
        public Texture2D tileset;

        [XmlIgnore]
        public LayerXml mainLayer;

        private void PostProcess(ContentManager Content)
        {
            //TILESET
            tileset = Content.Load<Texture2D>("Test/tileMapImg");

            //LAYERS
            foreach (LayerXml layer in Layers)
                if (layer.Name == "tiles")
                {
                    mainLayer = layer;
                    break;
                }

            //OBJECTS
            ObjectGroups[0].PostProcess();
        }


        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            foreach (LayerXml layer in Layers)
            {
                layer.Draw(this, gameTime, spriteBatch, position);
            }

        }

        public int getTileIndexAt(Vector2 pos)
        {
            int X = (int)(pos.X / TileWidth);
            int Y = (int)(pos.Y / TileHeight);
            if (X < 0 || Y < 0 || X >= Width || Y >= Height) return -1;
            return Y * Width + X;
        }

        public int getTileAt(Vector2 pos)
        {
            return getTileAt(getTileIndexAt(pos));
        }

        public int getTileAt(int index)
        {
            if (index < 0) return -1;
            //return datas[index];


            return mainLayer.Tiles[index].Gid;

            //return 0;
        }

        public Vector2 getTilePos(int index)
        {
            return new Vector2((index % Width) * TileWidth, (index / Width) * TileHeight);
        }

        public LayerXml GetLayerWithName(string name)
        {
            return null;
        }
    }


    public class TilesetXml
    {
        [XmlAttribute("source")]
        public string Source;
    }

    public class LayerXml
    {
        [XmlAttribute("name")]
        public string Name;
        
        [XmlArray("data"), XmlArrayItem("tile", typeof(TileXml))]
        public TileXml[] Tiles;

        internal void Draw(TiledMapXml map, GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            Vector2 pos = new Vector2();
            for (int x = 0; x < map.Width; x++)
                for (int y = 0; y < map.Height; y++)
                {
                    int i = y * map.Width + x;
                    pos.X = map.TileWidth * x;
                    pos.Y = map.TileHeight * y;

                    if(Tiles[i].Gid > 0)
                        DrawTile(map, gameTime, spriteBatch, pos - position, Tiles[i].Gid);
                }
        }

        void DrawTile(TiledMapXml map, GameTime gameTime, SpriteBatch spriteBatch, Vector2 position, int index)
        {
            Rectangle clip = new Rectangle(0, 0, 32, 32);
            clip.X = clip.Width * ((index - 1) % 8);
            clip.Y = clip.Height * ((index - 1) / 8);
            Vector2 pos = position;
            pos.X = (int)pos.X;
            pos.Y = (int)pos.Y;
            spriteBatch.Draw(map.tileset, pos, clip, Color.White);
        }
    }

    public class TileXml
    {
        [XmlAttribute("gid")]
        public Int32 Gid;
    }

    public class ObjectGroupXml
    {
        [XmlElement("object")]
        public ObjectXml[] Objects;

        [XmlIgnore]
        public Dictionary<string, ObjectXml> ObjectDict;

        public void PostProcess()
        {
            ObjectDict = new Dictionary<string,ObjectXml>();
            
            foreach (ObjectXml obj in Objects)
            {
                obj.PostProcess();

                ObjectDict[obj.Name] = obj;

            }
        }
    }

    public class ObjectXml
    {
        [XmlIgnore]
        public Object tag;
        
        [XmlAttribute("name")]
        public string Name;

        [XmlAttribute("type")]
        public string Type;
        
        [XmlAttribute("x")]
        public Int32 X;

        [XmlAttribute("y")]
        public Int32 Y;

        [XmlAttribute("width")]
        public Int32 Width;

        [XmlAttribute("height")]
        public Int32 Height;

        [XmlArray("properties"), XmlArrayItem("property", typeof(PropertyXml))]
        public PropertyXml[] PropertiesXml;


        [XmlIgnore]
        public Dictionary<string, string> Properties;

        public void PostProcess()
        {
            Properties = new Dictionary<string,string>();
            if (PropertiesXml == null) return;

            foreach(PropertyXml prop in PropertiesXml)
                Properties[prop.Name] = prop.Value;
        }

        /*
        public string GetValue(string propertyName)
        {
            foreach (PropertyXml prop in Properties)
                if (prop.Name == propertyName)
                    return prop.Value;
            return null;
        }*/
    }

    public class PropertyXml
    {
        [XmlAttribute("name")]
        public string Name;

        [XmlAttribute("value")]
        public string Value;
    }
}
