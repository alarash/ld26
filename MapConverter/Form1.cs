﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;


namespace MapConverter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.tmx|*.tmx";
            if (ofd.ShowDialog()!= DialogResult.OK) return;

            
            XmlDocument docTmx = new XmlDocument();
            docTmx.Load(ofd.FileName);

            createMap(docTmx);
            createTile(docTmx);
        }
        private void createMap(XmlDocument docTmx)
        {
            XmlDocument docMap = new XmlDocument();
            docMap.LoadXml(
                "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                "<XnaContent><Asset Type=\"LD26Lib.MapInfo\">" +
                    "<tileName>Toto</tileName><width/><height/><datas/><objects/>" +
                "</Asset></XnaContent>");

            XmlNode tmLayer = docTmx.SelectSingleNode("/map/layer");

            docMap.SelectSingleNode("/XnaContent/Asset/tileName").InnerText =
                textBox3.Text + textBox4.Text;
            docMap.SelectSingleNode("/XnaContent/Asset/width").InnerText = 
                tmLayer.Attributes["width"].Value;
            docMap.SelectSingleNode("/XnaContent/Asset/height").InnerText = 
                tmLayer.Attributes["height"].Value;

            String datas = "";
            foreach (XmlNode tile in tmLayer.SelectNodes("data/tile/@gid"))
                datas += " " + tile.Value;
            docMap.SelectSingleNode("/XnaContent/Asset/datas").InnerText = datas.Trim();

            

            XmlNode objs = docMap.SelectSingleNode("/XnaContent/Asset/objects");
            foreach(XmlNode obj in docTmx.SelectNodes("/map/objectgroup/object"))
            {
                XmlNode item = objs.AppendChild(docMap.CreateElement("Item"));
                item.InnerXml = "<Key></Key><Value><type/><width/><height/><x/><y/><properties/></Value>";
                item.SelectSingleNode("Key").InnerText = obj.Attributes["name"].Value;
                if (obj.Attributes["type"]!=null)
                    item.SelectSingleNode("Value/type").InnerText = obj.Attributes["type"].Value;
                item.SelectSingleNode("Value/width").InnerText = obj.Attributes["width"].Value;
                item.SelectSingleNode("Value/height").InnerText = obj.Attributes["height"].Value;
                item.SelectSingleNode("Value/x").InnerText = obj.Attributes["x"].Value;
                item.SelectSingleNode("Value/y").InnerText = obj.Attributes["y"].Value;

                foreach(XmlNode prop in obj.SelectNodes("properties/property"))
                {
                    XmlNode propItem = item.SelectSingleNode("Value/properties").AppendChild(docMap.CreateElement("Item"));
                    propItem.InnerXml = 
                        "<Key>" + prop.Attributes["name"].Value + "</Key>" + 
                        "<Value>" + prop.Attributes["value"].Value + "</Value>" ;
                }
            }

            textBox1.Text = docMap.OuterXml;
        }
        private void createTile(XmlDocument docTmx)
        {
            XmlDocument docTile = new XmlDocument();
            docTile.LoadXml(
                "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"+
                "<XnaContent><Asset Type=\"LD26Lib.TileSprite\">"+
                    "<Size/><spriteName/><nbByLine/><firstGid/>"+
                "</Asset></XnaContent>");

            XmlNode tmMap = docTmx.SelectSingleNode("/map/tileset");

            docTile.SelectSingleNode("/XnaContent/Asset/Size").InnerText = 
                tmMap.Attributes["tilewidth"].Value + " " +tmMap.Attributes["tileheight"].Value;

            docTile.SelectSingleNode("/XnaContent/Asset/spriteName").InnerText =
                textBox3.Text+removeExt(tmMap.SelectSingleNode("image/@source").Value);

            docTile.SelectSingleNode("/XnaContent/Asset/nbByLine").InnerText = 
                (int.Parse(tmMap.SelectSingleNode("image/@width").Value) /
                int.Parse(tmMap.SelectSingleNode("@tilewidth").Value)).ToString();

            docTile.SelectSingleNode("/XnaContent/Asset/firstGid").InnerText = 
                tmMap.SelectSingleNode("@firstgid").Value;

            textBox2.Text = docTile.OuterXml;
        }
        private String removeExt(String file)
        {
            int p = file.LastIndexOf('.');
            if (p < 0) return file;
            return file.Substring(0, p);
        }


        private void textBox1_DoubleClick(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "*.xml|*.xml";
            if (sfd.ShowDialog() != DialogResult.OK) return;

            StreamWriter sw = new StreamWriter(sfd.FileName);
            sw.Write(textBox1.Text);
            sw.Close();
        }

        private void textBox2_DoubleClick(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "*.xml|*.xml";
            if (sfd.ShowDialog() != DialogResult.OK) return;

            StreamWriter sw = new StreamWriter(sfd.FileName);
            sw.Write(textBox2.Text);
            sw.Close();
        }
    }
}
